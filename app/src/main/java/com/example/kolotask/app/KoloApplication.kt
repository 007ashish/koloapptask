package com.example.kolotask.app

import androidx.multidex.MultiDexApplication
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class KoloApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
    }

}