package com.example.kolotask.base

import androidx.appcompat.app.AppCompatActivity
import com.example.kolotask.util.Navigator
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

open class BaseActivity : AppCompatActivity() {

    @Inject
    lateinit var navigator: Navigator

}