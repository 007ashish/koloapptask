package com.example.kolotask.cache

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [RedditEntity::class], version = 1, exportSchema = false)
abstract class KoloDB : RoomDatabase() {

    companion object {
        const val DB_NAME = "KOLO_DB"
    }

    abstract fun mainDao(): MainDao
}