package com.example.kolotask.cache

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface MainDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun saveData(redditEntity: RedditEntity): Completable

    @Query("SELECT * FROM reddit_table where pageNo Like:pageNo")
    fun getData(pageNo: String): Single<RedditEntity>

    @Query("DELETE FROM reddit_table")
    fun clearData(): Completable

}