package com.example.kolotask.cache

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.kolotask.model.list.DataX
import com.example.kolotask.model.list.RedditListResponse
import com.google.gson.Gson

@Entity(tableName = "reddit_table")
data class RedditEntity(
    @PrimaryKey(autoGenerate = true) val id: Long? = null,
    val pageNo: String,
    val response: String
)

fun RedditListResponse.toRedditEntity(): RedditEntity {
    return RedditEntity(pageNo = this.data?.after ?: "", response = this.toGsonString())
}

fun RedditListResponse.toGsonString(): String {
    return Gson().toJson(this)
}

fun String.toRedditResponseObjectType(): RedditListResponse {
    return Gson().fromJson(this, RedditListResponse::class.java)
}

fun RedditEntity.toRedditResponse(): RedditListResponse {
    return this.response.toRedditResponseObjectType()
}

