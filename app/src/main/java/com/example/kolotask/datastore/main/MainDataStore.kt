package com.example.kolotask.datastore.main

import com.example.kolotask.model.list.RedditListResponse
import com.example.kolotask.ui.main.fragment.feeds.FeedFilterType
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface MainDataStore {

    fun fetchFeeds(
        limit: Int,
        after: String,
        filterType: FeedFilterType
    ): Single<RedditListResponse>

    fun searchFeeds(
        limit: Int,
        query: String
    ): Observable<RedditListResponse>

    fun saveFeeds(response: RedditListResponse): Completable

    fun clearAllCacheFeeds(): Completable

}