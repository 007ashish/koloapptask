package com.example.kolotask.datastore.main

import javax.inject.Inject

class MainDataStoreFactory @Inject constructor(
    private val mainRemoteDataStore: MainRemoteDataStore,
    private val mainLocalDataStore: MainLocalDataStore
) {

    fun getRemoteDataStore() = mainRemoteDataStore
    fun getLocalDataStore() = mainLocalDataStore

}