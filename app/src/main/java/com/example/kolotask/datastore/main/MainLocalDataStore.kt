package com.example.kolotask.datastore.main

import com.example.kolotask.cache.MainDao
import com.example.kolotask.cache.toRedditEntity
import com.example.kolotask.cache.toRedditResponse
import com.example.kolotask.model.list.RedditListResponse
import com.example.kolotask.ui.main.fragment.feeds.FeedFilterType
import com.example.kolotask.util.toAccessor
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import java.lang.Exception
import java.lang.UnsupportedOperationException
import javax.inject.Inject

class MainLocalDataStore @Inject constructor(private val mainDao: MainDao) : MainDataStore {

    override fun fetchFeeds(
        limit: Int,
        after: String,
        filterType: FeedFilterType
    ): Single<RedditListResponse> {
        return mainDao.getData(after)
            .flatMap {
                Single.just(it.toRedditResponse())
            }
    }


    override fun searchFeeds(limit: Int, query: String): Observable<RedditListResponse> {
        return Observable.error(UnsupportedOperationException("Not supported in local"))
    }

    override fun saveFeeds(response: RedditListResponse): Completable {
        return response.toRedditEntity().let { mainDao.saveData(it) }
    }

    override fun clearAllCacheFeeds(): Completable {
        return mainDao.clearData()
    }
}