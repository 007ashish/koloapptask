package com.example.kolotask.datastore.main

import com.example.kolotask.model.list.RedditListResponse
import com.example.kolotask.remote.MainService
import com.example.kolotask.ui.main.fragment.feeds.FeedFilterType
import com.example.kolotask.ui.main.fragment.feeds.FeedFilterType.*
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import java.lang.UnsupportedOperationException
import javax.inject.Inject

class MainRemoteDataStore @Inject constructor(private val mainService: MainService) :
    MainDataStore {

    override fun fetchFeeds(
        limit: Int,
        after: String,
        filterType: FeedFilterType
    ): Single<RedditListResponse> {
        return when (filterType) {
            HOT -> mainService.fetchHot(limit, after)
            TOP -> mainService.fetchTop(limit, after)
            RISING -> mainService.fetchRising(limit, after)
            New -> mainService.fetchNew(limit, after)
        }
    }

    override fun searchFeeds(limit: Int, query: String): Observable<RedditListResponse> {
        return mainService.search(limit, query)
    }

    override fun saveFeeds(response: RedditListResponse): Completable {
        return Completable.error(UnsupportedOperationException("Caching not supported in remote!"))
    }

    override fun clearAllCacheFeeds(): Completable {
        return Completable.error(UnsupportedOperationException("not supported in remote!"))
    }

}