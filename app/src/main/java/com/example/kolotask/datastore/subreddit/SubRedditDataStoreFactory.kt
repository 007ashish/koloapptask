package com.example.kolotask.datastore.subreddit

import javax.inject.Inject

class SubRedditDataStoreFactory @Inject constructor(
    private val localDataStore: SubRedditLocalDataStore,
    private val remoteDataStore: SubRedditRemoteDataStore
) {
    fun getLocalDataStore() = localDataStore
    fun getRemoteDataStore() = remoteDataStore
}