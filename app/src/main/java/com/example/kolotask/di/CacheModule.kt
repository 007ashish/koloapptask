package com.example.kolotask.di

import android.content.Context
import androidx.room.Room
import com.example.kolotask.cache.KoloDB
import com.example.kolotask.cache.MainDao
import com.example.kolotask.datastore.main.MainDataStore
import com.example.kolotask.datastore.main.MainLocalDataStore
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class CacheModule {

    @Provides
    @Singleton
    fun provideKoloDB(@ApplicationContext context: Context): KoloDB {
        return Room.databaseBuilder(context, KoloDB::class.java, KoloDB.DB_NAME).build()
    }

    @Provides
    @Singleton
    fun provideMainDao(koloDB: KoloDB): MainDao {
        return koloDB.mainDao()
    }

    @Provides
    @Singleton
    fun provideMainLocalDataStore(mainDao: MainDao): MainDataStore {
        return MainLocalDataStore(mainDao)
    }
}