package com.example.kolotask.di

import com.example.kolotask.datastore.main.MainDataStoreFactory
import com.example.kolotask.datastore.main.MainLocalDataStore
import com.example.kolotask.datastore.main.MainRemoteDataStore
import com.example.kolotask.datastore.subreddit.SubRedditDataStoreFactory
import com.example.kolotask.datastore.subreddit.SubRedditLocalDataStore
import com.example.kolotask.datastore.subreddit.SubRedditRemoteDataStore
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataModule {

    @Provides
    @Singleton
    fun provideMainDataStoreFactory(
        localDataStore: MainLocalDataStore,
        remoteDataStore: MainRemoteDataStore
    ): MainDataStoreFactory {
        return MainDataStoreFactory(remoteDataStore, localDataStore)
    }

    @Provides
    @Singleton
    fun provideSubRedditStoreFactory(
        localDataStore: SubRedditLocalDataStore,
        remoteDataStore: SubRedditRemoteDataStore
    ): SubRedditDataStoreFactory {
        return SubRedditDataStoreFactory(localDataStore, remoteDataStore)
    }


}