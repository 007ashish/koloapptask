package com.example.kolotask.di

import com.example.kolotask.datastore.main.MainDataStore
import com.example.kolotask.datastore.main.MainRemoteDataStore
import com.example.kolotask.remote.MainService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RemoteModule {

    @Provides
    @Singleton
    fun provideMainService(retrofit: Retrofit): MainService {
        return retrofit.create(MainService::class.java)
    }

    @Provides
    @Singleton
    fun provideMainRemoteDataStore(mainService: MainService): MainDataStore {
        return MainRemoteDataStore(mainService)
    }

}