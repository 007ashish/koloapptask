package com.example.kolotask.model.list

data class AuthorFlairRichtext(
    var e: String? = null,
    var t: String? = null
)