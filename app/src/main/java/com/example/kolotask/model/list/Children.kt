package com.example.kolotask.model.list

data class Children(
    var data: DataX? = null,
    var kind: String? = null
)