package com.example.kolotask.model.list

data class Data(
    var after: String? = null,
    var before: Any? = null,
    var children: List<Children>? = null,
    var dist: Int? = null,
    var geo_filter: Any? = null,
    var modhash: String? = null
)