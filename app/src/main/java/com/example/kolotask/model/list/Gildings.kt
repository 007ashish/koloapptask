package com.example.kolotask.model.list

data class Gildings(
    var gid_1: Int? = null,
    var gid_2: Int? = null
)