package com.example.kolotask.model.list

data class Image(
    var id: String? = null,
    var resolutions: List<Resolution>? = null,
    var source: Source? = null,
    var variants: Any? = null
)