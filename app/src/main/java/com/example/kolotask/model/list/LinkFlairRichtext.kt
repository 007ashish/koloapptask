package com.example.kolotask.model.list

data class LinkFlairRichtext(
    var a: String? = null,
    var e: String? = null,
    var t: String? = null,
    var u: String? = null
)