package com.example.kolotask.model.list

data class Preview(
    var enabled: Boolean? = null,
    var images: List<Image>? = null
)