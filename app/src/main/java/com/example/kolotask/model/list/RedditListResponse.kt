package com.example.kolotask.model.list

data class RedditListResponse(
    var data: Data? = null,
    var kind: String? = null
)