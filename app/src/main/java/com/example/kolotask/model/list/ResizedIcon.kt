package com.example.kolotask.model.list

data class ResizedIcon(
    var height: Int? = null,
    var url: String? = null,
    var width: Int? = null
)