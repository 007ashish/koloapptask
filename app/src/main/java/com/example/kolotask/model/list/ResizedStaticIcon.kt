package com.example.kolotask.model.list

data class ResizedStaticIcon(
    var height: Int? = null,
    var url: String? = null,
    var width: Int? = null
)