package com.example.kolotask.remote

import com.example.kolotask.model.list.RedditListResponse
import com.example.kolotask.util.Constants
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MainService {

    @GET("/r/popular/hot.json")
    fun fetchHot(
        @Query("limit") limit: Int,
        @Query("after") after: String
    ): Single<RedditListResponse>

    @GET("/r/popular/rising.json")
    fun fetchRising(
        @Query("limit") limit: Int,
        @Query("after") after: String
    ): Single<RedditListResponse>

    @GET("/r/popular/top.json")
    fun fetchTop(
        @Query("limit") limit: Int,
        @Query("after") after: String
    ): Single<RedditListResponse>

    @GET("/r/popular/new.json")
    fun fetchNew(
        @Query("limit") limit: Int,
        @Query("after") after: String = ""
    ): Single<RedditListResponse>


    @GET("/r/subreddit/search.json")
    fun search(
        @Query("limit") limit: Int,
        @Query("q") query: String
    ): Observable<RedditListResponse>


}