package com.example.kolotask.ui.main

import android.os.Bundle
import com.example.kolotask.base.BaseActivity
import com.example.kolotask.databinding.ActivityMainBinding
import com.example.kolotask.ui.main.adapter.MyPagerAdapter
import com.google.android.material.tabs.TabLayout
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseActivity(), MainActivityContractMVP.View {

    private lateinit var binding: ActivityMainBinding
    private lateinit var pagerAdapter: MyPagerAdapter

    @Inject
    lateinit var presenter: MainActivityContractMVP.Presenter

    companion object {
        private const val DEFAULT_TAB_SELECTED_POSITION = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupTabs()
    }

    private fun setupTabs() {
        pagerAdapter = MyPagerAdapter(supportFragmentManager)
        binding.viewPager.adapter = pagerAdapter
        binding.viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout))
        binding.tabLayout.setupWithViewPager(binding.viewPager)
        binding.viewPager.currentItem = DEFAULT_TAB_SELECTED_POSITION
    }
}