package com.example.kolotask.ui.main

import com.example.kolotask.base.BasePresenter
import com.example.kolotask.model.list.RedditListResponse
import com.example.kolotask.util.Constants
import io.reactivex.Single

interface MainActivityContractMVP {

    interface View {}

    interface Presenter : BasePresenter {}

    interface Model {}

}