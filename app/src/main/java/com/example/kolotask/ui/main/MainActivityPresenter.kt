package com.example.kolotask.ui.main

import com.example.kolotask.base.SchedulerProvider
import com.example.kolotask.model.list.RedditListResponse
import com.example.kolotask.util.log
import io.reactivex.SingleObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class MainActivityPresenter @Inject constructor(
    private val view: MainActivityContractMVP.View,
    private val model: MainActivityContractMVP.Model,
    private val schedulerProvider: SchedulerProvider
) : MainActivityContractMVP.Presenter {

    override fun onCreate() {

    }

    override fun onStart() {

    }

    override fun onResume() {

    }

    override fun onPause() {

    }

    override fun onStop() {

    }

    override fun onDestroy() {

    }

}