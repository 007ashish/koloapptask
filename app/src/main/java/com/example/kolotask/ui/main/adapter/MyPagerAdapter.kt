package com.example.kolotask.ui.main.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.kolotask.ui.main.fragment.feeds.PopularFeedFragment
import com.example.kolotask.ui.main.fragment.search.SearchFeedFragment
import java.lang.Exception

class MyPagerAdapter(fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount(): Int {
        return 2
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> PopularFeedFragment()
            1 -> SearchFeedFragment()
            else -> throw Exception(" Invalid Fragment Type!")
        }
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Popular Feeds"
            1 -> "Search Feeds"
            else -> ""
        }
    }

}