package com.example.kolotask.ui.main.di

import android.app.Activity
import com.example.kolotask.base.SchedulerProvider
import com.example.kolotask.datastore.main.MainDataStoreFactory
import com.example.kolotask.ui.main.MainActivity
import com.example.kolotask.ui.main.MainActivityContractMVP
import com.example.kolotask.ui.main.MainActivityModel
import com.example.kolotask.ui.main.MainActivityPresenter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
class MainActivityModule {

    @Provides
    fun provideMainActivity(activity: Activity): MainActivity {
        return activity as MainActivity
    }

    @Provides
    fun provideView(mainActivity: MainActivity): MainActivityContractMVP.View {
        return mainActivity
    }

    @Provides
    fun providePresenter(
        view: MainActivityContractMVP.View,
        model: MainActivityContractMVP.Model,
        schedulerProvider: SchedulerProvider
    ): MainActivityContractMVP.Presenter {
        return MainActivityPresenter(view, model, schedulerProvider)
    }

    @Provides
    fun provideModel(mainDataStoreFactory: MainDataStoreFactory): MainActivityContractMVP.Model {
        return MainActivityModel(mainDataStoreFactory)
    }

}