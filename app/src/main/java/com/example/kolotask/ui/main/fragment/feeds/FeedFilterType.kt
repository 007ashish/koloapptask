package com.example.kolotask.ui.main.fragment.feeds

enum class FeedFilterType(val title: String) {
    HOT(title = "HOT"),
    TOP(title = "TOP"),
    RISING(title = "Rising"),
    New(title = "New")
}