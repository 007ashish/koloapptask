package com.example.kolotask.ui.main.fragment.feeds

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kolotask.R
import com.example.kolotask.databinding.FragmentFeedListBinding
import com.example.kolotask.model.list.RedditListResponse
import com.example.kolotask.ui.main.fragment.feeds.FeedFilterType.*
import com.example.kolotask.ui.main.fragment.feeds.adapter.MyFeedAdapter
import com.example.kolotask.util.OnScrollListener
import com.example.kolotask.util.setVisibility
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class PopularFeedFragment : Fragment(), PopularFeedFragmentContractMVP.View {

    private lateinit var binding: FragmentFeedListBinding
    private lateinit var mContext: Context

    @Inject
    lateinit var presenter: PopularFeedFragmentPresenter
    private lateinit var myFeedAdapter: MyFeedAdapter
    private lateinit var onScrollListener: OnScrollListener
    private var filterType: FeedFilterType = HOT


    private val loadMoreLeadItems = object : OnScrollListener.LoadMoreItems {
        override fun onLoadMore() {
            myFeedAdapter.getCurrentPage().data?.let {
                presenter.loadFeeds(
                    after = it.after ?: "",
                    feedFilterType = filterType,
                    newFilter = false
                )
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFeedListBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onCreate()
        setupRecyclerView()

        // Feed Filter Type
        binding.ivFilterTypeAction.setOnClickListener {
            val popupMenu = PopupMenu(mContext, binding.ivFilterTypeAction)
            popupMenu.menuInflater.inflate(R.menu.popup_menu, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener { menuItem ->
                handleMenuItemAction(menuItem)
                true
            }
            popupMenu.show()
        }

        loadFreshFeeds(filterType = filterType)
    }

    private fun loadFreshFeeds(filterType: FeedFilterType) {
        presenter.loadFeeds(feedFilterType = filterType, newFilter = true)
        binding.tvFilterType.text = filterType.title
        onScrollListener.resetLoadStatus()
    }

    private fun handleMenuItemAction(menuItem: MenuItem) {
        when (menuItem.itemId) {
            R.id.hotFeed -> filterType = HOT
            R.id.topFeed -> filterType = TOP
            R.id.risingFeed -> filterType = RISING
            R.id.newFeed -> filterType = New
        }
        loadFreshFeeds(filterType)
    }

    override fun setupRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context)
        onScrollListener = OnScrollListener(linearLayoutManager, loadMoreLeadItems)
        myFeedAdapter = MyFeedAdapter()
        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = myFeedAdapter
            addOnScrollListener(onScrollListener)
        }
    }

    override fun loadData(data: RedditListResponse) {
        myFeedAdapter.loadData(data)
    }

    override fun toggleProgressBar(status: Boolean) {
        binding.progressBar.setVisibility(status)
    }

    override fun eraseAndLoadData(response: RedditListResponse) {
        myFeedAdapter.clearAllLoadData(response)
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

}