package com.example.kolotask.ui.main.fragment.feeds

import com.example.kolotask.base.BasePresenter
import com.example.kolotask.model.list.RedditListResponse
import io.reactivex.Single

interface PopularFeedFragmentContractMVP {

    interface View {
        fun setupRecyclerView()
        fun loadData(data: RedditListResponse)
        fun toggleProgressBar(status: Boolean)
        fun eraseAndLoadData(response: RedditListResponse)
    }

    interface Presenter : BasePresenter {
        fun loadFeeds(
            limit: Int = 25,
            after: String = "",
            feedFilterType: FeedFilterType = FeedFilterType.HOT,
            newFilter: Boolean = false
        )
    }

    interface Model {
        fun getFeedsObservable(
            limit: Int,
            after: String,
            feedFilterType: FeedFilterType
        ): Single<RedditListResponse>

    }

}