package com.example.kolotask.ui.main.fragment.feeds

import com.example.kolotask.datastore.main.MainDataStoreFactory
import com.example.kolotask.model.list.RedditListResponse
import com.example.kolotask.util.NetworkUtil
import com.example.kolotask.util.log
import io.reactivex.Single
import javax.inject.Inject

class PopularFeedFragmentModel @Inject constructor(
    private val mainDataStoreFactory: MainDataStoreFactory,
    private val networkUtil: NetworkUtil
) : PopularFeedFragmentContractMVP.Model {

    override fun getFeedsObservable(
        limit: Int,
        after: String,
        feedFilterType: FeedFilterType
    ): Single<RedditListResponse> {
        return mainDataStoreFactory.getRemoteDataStore()
            .fetchFeeds(limit, after, feedFilterType)
            .flatMap { response ->
                mainDataStoreFactory.getLocalDataStore().saveFeeds(response)
                    .andThen(Single.just(response))
            }.doOnError { it.log() }
    }
}