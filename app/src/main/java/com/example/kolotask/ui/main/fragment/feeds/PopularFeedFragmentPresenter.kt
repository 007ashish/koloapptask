package com.example.kolotask.ui.main.fragment.feeds

import com.example.kolotask.base.SchedulerProvider
import com.example.kolotask.model.list.RedditListResponse
import com.example.kolotask.util.log
import io.reactivex.SingleObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class PopularFeedFragmentPresenter @Inject constructor(
    private val view: PopularFeedFragmentContractMVP.View,
    private val model: PopularFeedFragmentContractMVP.Model,
    private val schedulerProvider: SchedulerProvider
) : PopularFeedFragmentContractMVP.Presenter {

    private val compositeDisposable = CompositeDisposable()


    override fun loadFeeds(
        limit: Int,
        after: String,
        feedFilterType: FeedFilterType,
        newFilter: Boolean
    ) {
        view.toggleProgressBar(true)
        model.getFeedsObservable(limit, after, feedFilterType)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.main())
            .subscribe(object : SingleObserver<RedditListResponse?> {
                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

                override fun onSuccess(response: RedditListResponse) {
                    if (newFilter) view.eraseAndLoadData(response) else view.loadData(response)
                    view.toggleProgressBar(false)
                }

                override fun onError(e: Throwable) {
                    e.log()
                    view.toggleProgressBar(false)
                }
            })
    }

    override fun onCreate() {

    }

    override fun onStart() {

    }

    override fun onResume() {

    }

    override fun onPause() {

    }

    override fun onStop() {

    }

    override fun onDestroy() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }


}