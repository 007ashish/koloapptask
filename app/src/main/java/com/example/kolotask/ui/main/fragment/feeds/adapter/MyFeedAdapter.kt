package com.example.kolotask.ui.main.fragment.feeds.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.kolotask.databinding.ItemFeedCardViewBinding
import com.example.kolotask.model.list.Children
import com.example.kolotask.model.list.RedditListResponse
import com.example.kolotask.util.loadImage

class MyFeedAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var currentPage = RedditListResponse()
    private val childrenData = mutableListOf<Children>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return FeedViewHolder(
            ItemFeedCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val currentData = childrenData[position]
        (holder as FeedViewHolder).bind(currentData)
    }

    override fun getItemCount(): Int {
        return childrenData.size
    }

    fun getCurrentPage() = currentPage

    fun loadData(page: RedditListResponse) {
        this.currentPage = page
        page.data?.children?.let {
            this.childrenData.addAll(it)
            notifyItemRangeInserted(itemCount, it.size)
        }
    }

    fun clearAllLoadData(page: RedditListResponse) {
        this.currentPage = page
        page.data?.children?.let {
            childrenData.clear()
            this.childrenData.addAll(it)
            notifyDataSetChanged()
        }
    }

    private inner class FeedViewHolder(private val binding: ItemFeedCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(currentData: Children) {
            binding.apply {
                with(currentData.data) {
                    ivFeed.loadImage(this?.thumbnail ?: "")
                    tvTitle.text = this?.title
                }
            }
        }

    }

}