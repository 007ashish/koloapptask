package com.example.kolotask.ui.main.fragment.feeds.di

import androidx.fragment.app.Fragment
import com.example.kolotask.base.SchedulerProvider
import com.example.kolotask.datastore.main.MainDataStoreFactory
import com.example.kolotask.ui.main.fragment.feeds.PopularFeedFragment
import com.example.kolotask.ui.main.fragment.feeds.PopularFeedFragmentContractMVP
import com.example.kolotask.ui.main.fragment.feeds.PopularFeedFragmentModel
import com.example.kolotask.ui.main.fragment.feeds.PopularFeedFragmentPresenter
import com.example.kolotask.util.NetworkUtil
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@Module
@InstallIn(FragmentComponent::class)
class PopularFeedFragmentModule {

    @Provides
    fun provideView(fragment: Fragment): PopularFeedFragmentContractMVP.View {
        return fragment as PopularFeedFragment
    }

    @Provides
    fun providePresent(
        view: PopularFeedFragmentContractMVP.View,
        model: PopularFeedFragmentContractMVP.Model,
        schedulerProvider: SchedulerProvider
    ): PopularFeedFragmentContractMVP.Presenter {
        return PopularFeedFragmentPresenter(view, model, schedulerProvider)
    }

    @Provides
    fun provideModel(
        mainDataStoreFactory: MainDataStoreFactory,
        networkUtil: NetworkUtil
    ): PopularFeedFragmentContractMVP.Model {
        return PopularFeedFragmentModel(mainDataStoreFactory, networkUtil)
    }

}