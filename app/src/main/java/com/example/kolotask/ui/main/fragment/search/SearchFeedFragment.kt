package com.example.kolotask.ui.main.fragment.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kolotask.databinding.FragmentFeedSearchBinding
import com.example.kolotask.model.list.RedditListResponse
import com.example.kolotask.ui.main.fragment.feeds.adapter.MyFeedAdapter
import com.example.kolotask.util.RxSearchObservable
import com.example.kolotask.util.setVisibility
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@AndroidEntryPoint
class SearchFeedFragment : Fragment(), SearchFeedFragmentContractMVP.View {

    private lateinit var binding: FragmentFeedSearchBinding
    private lateinit var myFeedAdapter: MyFeedAdapter

    @Inject
    lateinit var presenter: SearchFeedFragmentPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFeedSearchBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()

        presenter.addDisposable(RxSearchObservable.fromView(binding.searchView)
            .debounce(500, TimeUnit.MILLISECONDS)
            .filter { it.isNotEmpty() }
            .distinctUntilChanged()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { finalQuery -> presenter.search(query = finalQuery) })
    }

    override fun setupRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context)
        myFeedAdapter = MyFeedAdapter()
        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = myFeedAdapter
        }
    }

    override fun loadDataInRecyclerView(data: RedditListResponse) {
        myFeedAdapter.clearAllLoadData(data)
    }

    override fun toggleProgressBar(status: Boolean) {
        binding.progressBar.setVisibility(status)
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

}