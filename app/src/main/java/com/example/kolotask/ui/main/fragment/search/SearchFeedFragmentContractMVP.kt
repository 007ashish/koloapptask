package com.example.kolotask.ui.main.fragment.search

import com.example.kolotask.base.BasePresenter
import com.example.kolotask.model.list.RedditListResponse
import io.reactivex.Observable
import io.reactivex.disposables.Disposable

interface SearchFeedFragmentContractMVP {

    interface View {
        fun setupRecyclerView()
        fun loadDataInRecyclerView(data: RedditListResponse)
        fun toggleProgressBar(status: Boolean)
    }

    interface Presenter : BasePresenter {
        fun addDisposable(d: Disposable)
        fun search(limit: Int = 10, query: String)
    }

    interface Model {
        fun getFeeds(limit: Int, query: String): Observable<RedditListResponse>
    }

}