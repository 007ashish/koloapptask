package com.example.kolotask.ui.main.fragment.search

import com.example.kolotask.datastore.main.MainDataStoreFactory
import com.example.kolotask.model.list.RedditListResponse
import com.example.kolotask.util.NetworkUtil
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class SearchFeedFragmentModel @Inject constructor(
    private val mainDataStoreFactory: MainDataStoreFactory,
    networkUtil: NetworkUtil
) : SearchFeedFragmentContractMVP.Model {

    override fun getFeeds(limit: Int, query: String): Observable<RedditListResponse> {
        return mainDataStoreFactory.getRemoteDataStore().searchFeeds(limit, query)
    }


}