package com.example.kolotask.ui.main.fragment.search

import com.example.kolotask.base.SchedulerProvider
import com.example.kolotask.model.list.RedditListResponse
import com.example.kolotask.util.log
import io.reactivex.Observer
import io.reactivex.SingleObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchFeedFragmentPresenter @Inject constructor(
    private val view: SearchFeedFragmentContractMVP.View,
    private val model: SearchFeedFragmentContractMVP.Model,
    private val schedulerProvider: SchedulerProvider
) : SearchFeedFragmentContractMVP.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun addDisposable(d: Disposable) {
        compositeDisposable.add(d)
    }

    override fun search(limit: Int, query: String) {
        view.toggleProgressBar(true)
        model.getFeeds(limit, query)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.main())
            .distinctUntilChanged()
            .subscribe(object : Observer<RedditListResponse> {
                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

                override fun onError(e: Throwable) {
                    e.log()
                    view.toggleProgressBar(false)
                }

                override fun onNext(response: RedditListResponse) {
                    view.loadDataInRecyclerView(response)
                }

                override fun onComplete() {
                    view.toggleProgressBar(false)
                }
            })
    }

    override fun onCreate() {
    }

    override fun onStart() {
    }

    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun onStop() {
    }

    override fun onDestroy() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }
}