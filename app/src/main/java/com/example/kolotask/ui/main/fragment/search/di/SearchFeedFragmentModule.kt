package com.example.kolotask.ui.main.fragment.search.di

import androidx.fragment.app.Fragment
import com.example.kolotask.base.SchedulerProvider
import com.example.kolotask.datastore.main.MainDataStoreFactory
import com.example.kolotask.ui.main.fragment.feeds.PopularFeedFragment
import com.example.kolotask.ui.main.fragment.feeds.PopularFeedFragmentContractMVP
import com.example.kolotask.ui.main.fragment.search.SearchFeedFragment
import com.example.kolotask.ui.main.fragment.search.SearchFeedFragmentContractMVP
import com.example.kolotask.ui.main.fragment.search.SearchFeedFragmentModel
import com.example.kolotask.ui.main.fragment.search.SearchFeedFragmentPresenter
import com.example.kolotask.util.NetworkUtil
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@Module
@InstallIn(FragmentComponent::class)
class SearchFeedFragmentModule {

    @Provides
    fun provideView(fragment: Fragment): SearchFeedFragmentContractMVP.View {
        return fragment as SearchFeedFragment
    }

    @Provides
    fun providePresent(
        view: SearchFeedFragmentContractMVP.View,
        model: SearchFeedFragmentContractMVP.Model,
        schedulerProvider: SchedulerProvider
    ): SearchFeedFragmentContractMVP.Presenter {
        return SearchFeedFragmentPresenter(view, model, schedulerProvider)
    }

    @Provides
    fun provideModel(
        mainDataStoreFactory: MainDataStoreFactory,
        networkUtil: NetworkUtil
    ): SearchFeedFragmentContractMVP.Model {
        return SearchFeedFragmentModel(mainDataStoreFactory, networkUtil)
    }


}