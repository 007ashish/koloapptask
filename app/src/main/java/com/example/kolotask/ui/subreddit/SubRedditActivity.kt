package com.example.kolotask.ui.subreddit

import android.os.Bundle
import com.example.kolotask.base.BaseActivity
import com.example.kolotask.databinding.ActivitySubRedditBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SubRedditActivity : BaseActivity(), SubRedditActivityContractMVP.View {

    private lateinit var binding: ActivitySubRedditBinding

    @Inject
    lateinit var presenter: SubRedditActivityContractMVP.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySubRedditBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}