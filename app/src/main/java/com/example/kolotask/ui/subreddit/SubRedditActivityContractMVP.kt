package com.example.kolotask.ui.subreddit

interface SubRedditActivityContractMVP {

    interface View {}
    interface Presenter {}
    interface Model {}

}