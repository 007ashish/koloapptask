package com.example.kolotask.ui.subreddit

import com.example.kolotask.datastore.subreddit.SubRedditDataStoreFactory
import javax.inject.Inject

class SubRedditActivityModel @Inject constructor(private val subRedditDataStoreFactory: SubRedditDataStoreFactory) :
    SubRedditActivityContractMVP.Model {
}