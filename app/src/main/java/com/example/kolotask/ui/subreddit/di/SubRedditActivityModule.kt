package com.example.kolotask.ui.subreddit.di

import android.app.Activity
import com.example.kolotask.datastore.subreddit.SubRedditDataStoreFactory
import com.example.kolotask.ui.subreddit.SubRedditActivity
import com.example.kolotask.ui.subreddit.SubRedditActivityContractMVP
import com.example.kolotask.ui.subreddit.SubRedditActivityModel
import com.example.kolotask.ui.subreddit.SubRedditActivityPresenter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
class SubRedditActivityModule {

    @Provides
    fun provideSubRedditActivity(activity: Activity): SubRedditActivity {
        return activity as SubRedditActivity
    }

    @Provides
    fun provideView(subRedditActivity: SubRedditActivity): SubRedditActivityContractMVP.View {
        return subRedditActivity
    }

    @Provides
    fun providePresenter(
        view: SubRedditActivityContractMVP.View,
        model: SubRedditActivityContractMVP.Model
    ): SubRedditActivityContractMVP.Presenter {
        return SubRedditActivityPresenter(view, model)
    }

    @Provides
    fun provideModel(subRedditDataStoreFactory: SubRedditDataStoreFactory): SubRedditActivityContractMVP.Model {
        return SubRedditActivityModel(subRedditDataStoreFactory)
    }

}