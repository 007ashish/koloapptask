package com.example.kolotask.util

import android.util.Log
import android.view.View

private const val app_log = "kolo_log"

fun String.log(initial: String? = null, tag: String? = app_log) {
    val msgInitial = if (initial != null) "$initial: " else ""
    Log.d(tag, "$msgInitial$this")
}

fun Any.log(initial: String? = null, tag: String? = app_log) {
    val msgInitial = if (initial != null) "$initial: " else ""
    Log.d(tag, "$msgInitial$this")
}

fun View.setVisibility(status: Boolean) {
    if (status) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

/**
 * Handles Null in Single<T> of RxJava as RxJava 2.0 doesn't support null values
 */
interface Accessor<out T> {
    fun get(): T
}

fun <T> T.toAccessor() = object : Accessor<T> {
    override fun get(): T {
        return this@toAccessor
    }
}